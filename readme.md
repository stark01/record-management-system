<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


## FUNCTION OF USERS

## Admin
	-Upload CSV file in the system for the information of the student
	-Approve grades submitted by a teacher
	-Create Academic year for the system
	-Edit and View of the student profile
	-Generate reports such as Form 137, Report Card, and Reports on Promotion
	-Set the section, tracks, strands, year level subject assign to the teacher, assigning of students to the section, curriculum.
	-Manage the subject components.
	-Set the submenus for the subject.
	-View the suggestive grade of the student; this function will be triggered by the button.

## Teacher
	-Encode grades
	-Separate the list of advisory class and student subject.
	-Input weekly attendance
	-Generate Report Card

## Parent
	-Received an email for the performance of the children.
	-View the grades and attendance of the student.
	-View the suggestive grade given by the school.
