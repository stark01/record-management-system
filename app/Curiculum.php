<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Curiculum extends Model
{
	protected $guarded = [];

    public function save_curiculum($data){
    	if ($data) {
    		$this->create($data);
    		return true;
    	}
    }

    public function get_cuciculum_per_strand($strand_id){
    	if ($strand_id) {
    		  return $this->join('strands','strands.id','=','curiculums.strand_id')
                ->join('levels','levels.id','=','curiculums.level_id')
                ->join('semesters','semesters.id','=','curiculums.sem_id')
                ->join('subject_types','subject_types.id','=', 'curiculums.subject_type_id')
                ->join('subjects','subjects.id', '=','curiculums.subject_id')
                ->where('curiculums.strand_id', $strand_id)
                ->get();
    	}
    }
}
