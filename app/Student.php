<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Student extends Model
{
	protected $table = 'student';
    protected $guarded = [];
    
    public function get_all_student(){
    	return $this->join('levels','levels.id','=','student.level_id')
    				->join('semesters','semesters.id', '=','student.sem_id')
    				->get();
    }

    public function save_student($data){
    	$this->create($data);
    	return true;
    }


    public function get_profile($student_id){
        return $this->join('levels','levels.id','=','student.level_id')
                    ->join('semesters','semesters.id', '=','student.sem_id')
                    ->where('student_id', $student_id)
                    ->get();
    }

}
