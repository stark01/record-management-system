<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Redirect;
use Session;


class LoginController extends Controller
{
    public function __construct(){
		$this->user = new User;
        Session::start();
    }

    public function login(){
    	if (Auth::check()) {
	        $userRole = Auth::user()->role->role_name;
	            
	            if ($userRole == 'admin') {
	                return redirect()->route('admin');
	            } else {
	                abort(404);
	            }
	        }
	   	return view('auth.login');
	}

	 public function login_validate(Request $request){
        $this->validation($request);
        if (Auth::attempt([ 'email'=> $request->email,'password' => $request->password])) {
            $user = Auth::user();
                if ($user->role->role_name == 'admin') {
                    return redirect()->route('admin');
            	}else {
	            	return redirect()->route('login');
	        	}
            } else {
                return redirect()->route('login');

            }
    	}

	public function logout(){
        Session::flush();
        Auth::logout();
        return redirect('/');
    }

     public function validation($request){
        return $this->validate($request, [
                'email' => 'required',
                'password'  => 'required|max:255']);
    }
    
}
