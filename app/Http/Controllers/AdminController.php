<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\StudentImport;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use App\Role;
use App\Student;
use App\Academic_year;
use App\Strand;
use App\Level;
use App\Semester;

class AdminController extends Controller
{
    public function __construct(){
    	$this->user = new User;
    	$this->role = new Role;
        $this->acad_year = new Academic_year;
        $this->strand = new Strand;
        $this->level = new Level;
        $this->sem = new Semester;
    }

     public function user_form(){
    	$roles = $this->role->get_role();
    	return view('admin.add-user',compact('roles'));
    }

    public function save_user(Request $request){
    	if ($request) {
    		$data = [
    			'email'      => $request->email,
    			'name'       => $request->firstname.' '.$request->middle_name.' '.$request->lastname,
    			'password'   => bcrypt('lvcs@'.strtolower($request->lastname)),
    			'role_id'    => (int)$request->role_id
    		];
    		
            if ($this->user->save_user($data)) {
    			return redirect()->route('user_form');
    		} else {
	            abort(404);
    		}
    	}
    }

    public function import_blade(){
        return view('admin.import-view');
    }

    public function importing(){
        Excel::import(new StudentImport,request()->file('file'));   
        return redirect()->route('import_blade');
    }

    public function academic_year_form(){
        $academic_year = $this->acad_year->get_acad_year();
        return view('admin.add-academic-year',compact('academic_year'));
    }

    public function save_academic_year(Request $request){
        $data = [
            'academic_year' => $request->acad_year
        ];
        if ($this->acad_year->save_academic_year($data)) {
            return redirect()->route('academic_year_form');
        } else {
            abort(404);
        }
    }

    public function activate_ac_year($id){
        if ($this->acad_year->activate_ac_year($id)) {
            return redirect()->route('academic_year_form');
        }
    }

    public function add_student(){
        $semesters = $this->sem->get_semester();
        $levels = $this->level->get_level();
        $strands = $this->strand->get_strand();
        return view('admin.students.add-student',compact('strands','levels','semesters'));
    }

}
