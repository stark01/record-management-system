<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Strand;
use App\Level;
use App\Subject_type;
use App\Semester;
use App\Subject;
use App\Curiculum;
use DB;

class CuriculumController extends Controller
{
    public function __construct(){
    	$this->strand = new Strand;
    	$this->level = new Level;
    	$this->subject_type = new Subject_type;
    	$this->semester = new Semester;
        $this->subject = new Subject;
        $this->curiculum = new Curiculum;
    }

    public function curiculum(){
        $strands = $this->strand->get_strand();
        return view('admin.curiculum.strand', compact('strands'));
    }

    public function add_curiculum_form(){
    	$strands = $this->strand->get_strand();
    	$levels = $this->level->get_level();
    	$s_type = $this->subject_type->get_subject_type();
    	$semesters = $this->semester->get_semester();
    	return view('admin.curiculum.add-curiculum',compact('strands', 'levels', 's_type', 'semesters'));
    }

    public function get_subject_by_type($type){
        $subjects = $this->subject->get_subject_by_type($type);
        return response()->json($subjects);
    }

    public function save_curiculum(Request $request){
        if ($request) {
            $data = [
                'strand_id' => $request->strand_id,
                'level_id' => $request->level_id,
                'sem_id' => $request->sem_id,
                'subject_type_id' => $request->subject_type_id,
                'subject_id' => $request->subject_id,
            ];

            if ($this->curiculum->save_curiculum($data)) {
                return redirect()->route('curiculum_form');
            }
        }
    }

    public function get_curiculum($strand_id){
      if ($strand_id) {
          $curiculum = $this->curiculum->get_cuciculum_per_strand($strand_id);
            return view('admin.curiculum.curiculum',compact('curiculum'));
      }
    }
}
