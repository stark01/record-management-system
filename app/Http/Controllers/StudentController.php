<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function __construct(){
    	$this->student = new Student;
    }

    public function student_list(){
    	$students = $this->student->get_all_student();
    	return view('admin.students.students',compact('students'));
    }

    public function save_student(Request $request){
    	if ($request) {
    		$data = [
    			'student_id' => $request->student_id,
    			'email_s' => $request->email,
    			'firstname' => $request->firstname,
    			'middle_name' => $request->middle_name,
    			'lastname' => $request->lastname,
    			'birthdate' => $request->birthdate,
    			'sem_id' => $request->sem_id,
    			'level_id' => $request->level_id,
    			'strand_id' => $request->strand_id,
    			'guardian_no' => $request->guardian_no,
    		];

    		$this->student->save_student($data);
    		return redirect()->route('add_student');
    	}
    }

    public function student_profile($student_id){
        $data = $this->student->get_profile($student_id);
        return view('admin.students.profile',compact('data'));
    }
}
