<?php

namespace App\Imports;

use App\Student;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentImport implements ToModel ,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $rows)
    {

        return new Student([
            'student_id' => $rows['student_id'],
            'firstname' => $rows['firstname'],
            'middle_name' => $rows['middlename'],
            'lastname' => $rows['lastname'],
            'birthdate' => $rows['birthdate'],
            'mother_name' => $rows['mother_name'],
            'father_name' => $rows['father_name'],
            'parent_no' => $rows['parent_no'],
        ]);
    }
}
