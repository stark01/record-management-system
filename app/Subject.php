<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded = [];

    public function get_subject_by_type($type){
    	if ($type) {
    		return $this->where('subject_type', $type)->get();
    	}
    }
}
