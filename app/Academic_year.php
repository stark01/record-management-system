<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Academic_year extends Model
{
    protected $table = 'academic_year';
    protected $guarded = [];


    public function get_acad_year(){
    	return $this->all();
    }

    public function save_academic_year($data){
    	if ($data) {
    		$this->create($data);
    		return true;
    	}
    }

    public function activate_ac_year($id){
    	if ($id) {
    		$find = $this->findOrFail($id);
    		$find->update([
    			'status' => 'Active'
    		]);
    		return true;
    	}
    }
}
