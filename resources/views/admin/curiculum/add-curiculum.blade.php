@extends('layouts.app-admin')

@section('content')
<div class="col-lg-12 ">
    <div class="card">
        <div class="card-header" style="background-color: #212529;color: white"><strong>Create Curiculum</strong></div>
        <div class="card-body card-block">
        	<form method="POST" action="{{ Route('save_curiculum') }}">
                @csrf
	            <div class="form-group">
	            	<label for="fullname" class=" form-control-label">Select Strand</label>
	            	<select class="form-control" name="strand_id" required>
	            		@if($strands)
	            				<option value="">-- Select Strand --</option>
	            			@foreach($strands as $str)
	            				<option value="{{ $str->id }}">{{ $str->strand_name }}</option>
	            			@endforeach
	            		@endif
	            	</select>
	            </div>
	            <div class="form-group">
	            	<label for="fullname" class=" form-control-label">Levels</label>
	            	<select class="form-control" name="level_id" required>
	            		@if($levels)
	            			<option value="" >-- Select Level --</option>
	            			@foreach($levels as $level)
	            				<option value="{{ $level->id }}" >{{ $level->level_name }}</option>
	            			@endforeach
	            		@endif
	            	</select>
	            </div>
	            <div class="form-group">
	            	<label for="fullname" class=" form-control-label">Semester</label>
	            	<select class="form-control" name="sem_id" required>
	            		@if($semesters)
	            			<option value="" >-- Select Semester --</option>
	            			@foreach($semesters as $sem)
	            				<option value="{{ $sem->id }}">{{ $sem->semester_name }}</option>
	            			@endforeach
	            		@endif
	            	</select>
	            </div>
	            <div class="form-group">
	            	<label for="fullname" class=" form-control-label">Subject Type</label>
	            	<select class="form-control" id="subject_type_id" name="subject_type_id" url="{{ Route('get_subject','') }}" required>
	            		@if($s_type)
	            			<option value="" >-- Select Subject Type -- </option>
	            			@foreach($s_type as $type)
	            				<option value="{{ $type->id }}">{{ $type->subject_type }}</option>
	            			@endforeach
	            		@endif
	            	</select>
	            </div>

	            <div class="form-group">
	            	<label for="fullname" class=" form-control-label">Subjects</label>
	            	<select class="form-control" id="subjects" name="subject_id"  required>
	            		<!-- AJAX -->
	            	</select>
	            </div>
	            
	            <div class="form-group pull-right">
	            	<button type="submit" class="btn btn-success">Save</button>	
	            </div>
        	</form>
        </div>
    </div>
</div>

@endsection