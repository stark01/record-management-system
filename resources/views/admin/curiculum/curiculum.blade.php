@extends('layouts.app-admin')
@section('content')
<div class="col-lg-12 ">
    <div class="card">
        <div class="card-header" style="background-color: #212529;color: white"><strong>Curiculum</strong></div>
        <div class="card-body card-block">
        	<table class="table table-hover table-striped ">
        		<thead align="center">
        			<th colspan="3">GRADE 11</th>
        		</thead>
        		<tbody>
        			<tr style="background-color: #212529;color: white">
        				<td align="center">1ST SEMESTER</td>
        				<td></td>
        				<td align="center">2ND SEMESTER</td>
        			</tr>
        			
        			<tr>
        				<td>
	        				<table class="table table-bordered">
        						<th>Subjects</th>
        						<th>Hours</th>
        						@if($curiculum)
        							@foreach($curiculum as $cur)
        								@if($cur->semester_name == '1st Semester' && $cur->level_name == 'Grade 11')
        									<tr>
        										<td>{{ $cur->subject_name }}</td>
        										<td>{{ $cur->subject_hours }}</td>
        									</tr>
        								@endif
        							@endforeach
        						@endif
	        				</table>
        				</td>
        				<td width=""></td>
        				<td>
	        				<table class="table table-bordered">
        						<th>Subjects</th>
        						<th>Hours</th>
        						@if($curiculum)
        							@foreach($curiculum as $cur)
        								@if($cur->semester_name == '2nd Semester' && $cur->level_name == 'Grade 11')
        									<tr>
        										<td>{{ $cur->subject_name }}</td>
        										<td>{{ $cur->subject_hours }}</td>
        									</tr>
        								@endif
        							@endforeach
        						@endif
	        				</table>
        				</td>
        			</tr>
        		</tbody>
        	</table>

        	<hr>
        	<!-- SEPARATION -->
        	<hr>

        	<table class="table table-hover table-striped ">
        		<thead align="center">
        			<th colspan="3">GRADE 12</th>
        		</thead>
        		<tbody>
        			<tr style="background-color: #212529;color: white">
        				<td align="center">1ST SEMESTER</td>
        				<td></td>
        				<td align="center">2ND SEMESTER</td>
        			</tr>
        			
        			<tr>
        				<td>
	        				<table class="table table-bordered">
        						<th>Subjects</th>
        						<th>Hours</th>
        						@if($curiculum)
        							@foreach($curiculum as $cur)
        								@if($cur->semester_name == '1st Semester' && $cur->level_name == 'Grade 12')
        									<tr>
        										<td>{{ $cur->subject_name }}</td>
        										<td>{{ $cur->subject_hours }}</td>
        									</tr>
        								@endif
        							@endforeach
        						@endif
	        				</table>
        				</td>
        				<td width=""></td>
        				<td>
	        				<table class="table table-bordered">
        						<th>Subjects</th>
        						<th>Hours</th>
        						@if($curiculum)
        							@foreach($curiculum as $cur)
        								@if($cur->semester_name == '2nd Semester' && $cur->level_name == 'Grade 12')
        									<tr>
        										<td>{{ $cur->subject_name }}</td>
        										<td>{{ $cur->subject_hours }}</td>
        									</tr>
        								@endif
        							@endforeach
        						@endif
	        				</table>
        				</td>
        			</tr>
        		</tbody>
        	</table>
        </div>
    </div>
</div>
@endsection