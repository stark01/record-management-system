@extends('layouts.app-admin')
@section('content')
<div class="col-lg-12 ">
    <div class="card">
        <div class="card-header" style="background-color: #212529;color: white"><strong>Courses</strong></div>
        <div class="card-body card-block">
        	<table class="table table-hover table-striped">
        		<thead>
        			<th>Code</th>
        			<th>Strand</th>
        			<th>Action</th>
        		</thead>
        		<tbody>
        			@if($strands)
        				@foreach($strands as $str)
		        			<tr>
		        				<td>{{ $str->strand_code }}</td>	
		        				<td>{{ $str->strand_name }}</td>	
		        				<td>
		        					<a href="{{ route('get_curiculum', $str->id) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
		        				</td>	
		        			</tr>
		        		@endforeach
        			@endif
        		</tbody>
        	</table>
        </div>
    </div>
</div>
@endsection