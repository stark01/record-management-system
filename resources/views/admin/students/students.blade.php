@extends('layouts.app-admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="background-color: #212529;color: white">Students
                	<span class="pull-right">
                		<input type="" name="" class="form-control" placeholder="Search Student">
                	</span>
                </div>
                <div class="card-body">
                   <div class="col-md-12">
    	           		<div class="table-stats order-table ov-h">
                            <table class="table ">
                                <thead>
                                    <tr>
                                    	<th>Student ID</th>
               	                        <th>Student Name</th>
                                        <th>Level</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($students)
                                    	@foreach($students as $stud)
                                    		<tr>
                                    			<td><i>{{ $stud->student_id }}</i></td>
                                    			<td>{{ $stud->firstname }} {{ $stud->middle_name }} {{ $stud->lastname }}</td>
                                    			<td>{{ $stud->level_name }}</td>
                                    			<td>
                                    				<a href="{{ route('student_profile' , $stud->student_id) }}" class="btn btn-sm btn-default"><i class="fa fa-user"></i></a>
                                    			</td>
                                    		</tr>
                                    	@endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
	           		</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
