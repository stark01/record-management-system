@extends('layouts.app-admin')

@section('content')
<div class="col-lg-12">
    <div class="card">
        <div class="card-header" style="background-color: #212529;color: white"><strong>New Student</strong></div>
        <div class="card-body card-block">
        	<form method="POST" action="{{ route('save_student') }}">
                @csrf
                <div class="row">
                	<div class="col-md-12">
                			
			            <label for="student_id" class=" form-control-label"><i>Student ID</i></label>
                		<input id="student_id" class="form-control" type="text" name="student_id" style="width: 30%" placeholder="--------" required>
                		<!-- <button class="btn btn-default btn-sm">Generate</button> -->
                		<br>
                	</div>
                	<div class="col-md-4">
			            <div class="form-group">
			            	<label for="fullname" class=" form-control-label">First Name</label>
			            	<input type="text" id="fullname" name="firstname" class="form-control" required>
			            </div>
                	</div>
                	<div class="col-md-4">
			            <div class="form-group">
			            	<label for="fullname" class=" form-control-label">Middle Name</label>
			            	<input type="text" id="fullname" name="middle_name" class="form-control" required>
			            </div>
                	</div>
                	<div class="col-md-4">
			            <div class="form-group">
			            	<label for="fullname" class=" form-control-label">Last Name</label>
			            	<input type="text" id="fullname" name="lastname" class="form-control" required>
			            </div>
                	</div>
		            <div class="col-md-4">
		            	<div class="form-group">
			            	<label for="email" class=" form-control-label">Email</label>
			            	<input type="email" id="email" name="email" placeholder="example@email.com" class="form-control" required>
			            </div>
		            </div>
		            <div class="col-md-4">
		            	<div class="form-group">
			            	<label for="email" class=" form-control-label">Guardian Number</label>
			            	<input type="text" name="guardian_no" class="form-control" required>
			            </div>
		            </div>
		            <div class="col-md-4">
		            	<div class="form-group">
			            	<label for="email" class=" form-control-label">Birth Date</label>
			            	<input type="date" name="birthdate" class="form-control" required>
			            </div>
		            </div>
		            <div class="col-md-4">
			            <div class="form-group">
			            	<label for="access-type" class=" form-control-label">Level</label>
			            	<select class="form-control" name="level_id" required>
			            		@if($levels)
			            			<option value="" >-- Select Level --</option>
			            			@foreach($levels as $level)
			            				<option value="{{ $level->id }}" >{{ $level->level_name }}</option>
			            			@endforeach
			            		@endif
			            	</select>
			            </div>
		            </div>
		            <div class="col-md-4">
			            <div class="form-group">
			            	<label for="access-type" class=" form-control-label">Semester</label>
			            	<select class="form-control" name="sem_id" required>
			            		@if($semesters)
			            			<option value="" >-- Select Semester --</option>
			            			@foreach($semesters as $sem)
			            				<option value="{{ $sem->id }}" >{{ $sem->semester_name }}</option>
			            			@endforeach
			            		@endif
			            	</select>
			            </div>
		            </div>
		            <div class="col-md-6">
			            <div class="form-group">
			            	<label for="access-type" class=" form-control-label">Strands</label>
			            	<select class="form-control" name="strand_id" required>
			            		@if($strands)
			            			<option value="" >-- Select Strand --</option>
			            			@foreach($strands as $strand)
			            				<option value="{{ $strand->id }}" >{{ $strand->strand_name }}</option>
			            			@endforeach
			            		@endif
			            	</select>
			            </div>
		            </div>
		            <div class="col-md-12">
			            <div class="form-group" align="center">
			            	<button type="submit" class="btn btn-success">Confirm Registration</button>	
			            </div>
		            </div>
                </div>
        	</form>
        </div>
    </div>
</div>

@endsection