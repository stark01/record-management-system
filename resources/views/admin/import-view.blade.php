@extends('layouts.app-admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="background-color: #212529;color: white"> <span class="fa fa-archive"></span> Import form Excel</div>
                <div class="card-body">
                    <form action="{{ route('importing') }}" method="POST" enctype="multipart/form-data">
                    	 @csrf
		                <input type="file" name="file" class="form-control">
		                <br>
		                <button class="btn btn-success">Import Data </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection