@extends('layouts.app-admin')

@section('content')
<div class="col-lg-12">
    <div class="card">
        <div class="card-header" style="background-color: #212529;color: white"><strong>Set Academic Year</strong></div>
        <div class="card-body card-block">
        	<form method="POST" action="{{ Route('save_academic_year') }}">
                @csrf
	            <div class="row form-group">
                    <div class="col col-md-6">
                        <div class="input-group">
                            <input type="text"  name="acad_year" placeholder="Ex: 2018-2019" class="form-control" required>
                            <div class="input-group-btn">
                            	<button class="btn btn-success">Save Academic Year</button>
                            </div>
                        </div>
                    </div>
                </div>
        	</form>
        	<div class="col-lg-12">
                <div class="card">
                    <div class="table-stats order-table ov-h">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Academic Year</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@if($academic_year)
                            		@foreach($academic_year as $a)
			                            <tr>
			                                <td>{{ $a->id }}</td>
			                                <td> <span>{{ $a->academic_year }}</span> </td>
			                                <td>
			                                	@if($a->status)
			                                    	<span class="badge badge-complete">Active</span>
			                                	@endif
			                                </td>
			                                <td>
			                                	@if($a->status != 'Active')
				                                	<a href="{{ route('activate_ac_year',$a->id) }}">
				                                		<span class="badge badge-pending">Activate</span>
				                                	</a>
			                                	@endif
			                                </td>
			                            </tr>
		                            @endforeach
		                        @else
		                        <tr>
	                                <td>No Records </td>
	                                <td></td>
	                                <td></td>
	                                <td></td>
	                            </tr>
		                        @endif
                            </tbody>
                        </table>
                    </div> <!-- /.table-stats -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection