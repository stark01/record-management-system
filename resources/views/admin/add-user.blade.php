@extends('layouts.app-admin')

@section('content')
<div class="col-lg-8 offset-md-2">
    <div class="card">
        <div class="card-header" style="background-color: #212529;color: white"><strong>New User</strong></div>
        <div class="card-body card-block">
        	<form method="POST" action="{{ Route('save_user') }}">
                @csrf
	            <div class="form-group">
	            	<label for="fullname" class=" form-control-label">First Name</label>
	            	<input type="text" id="fullname" name="firstname" class="form-control" required>
	            </div>
	            <div class="form-group">
	            	<label for="fullname" class=" form-control-label">Middle Name</label>
	            	<input type="text" id="fullname" name="middle_name" class="form-control" required>
	            </div>
	            <div class="form-group">
	            	<label for="fullname" class=" form-control-label">Last Name</label>
	            	<input type="text" id="fullname" name="lastname" class="form-control" required>
	            </div>
	            <div class="form-group">
	            	<label for="email" class=" form-control-label">Email</label>
	            	<input type="email" id="email" name="email" placeholder="example@email.com" class="form-control" required>
	            </div>
	            <div class="form-group">
	            	<label for="access-type" class=" form-control-label">Access Type</label>
	            	<select class="form-control" name="role_id">
	            		@foreach($roles as $role)
	            			<option value="{{ $role->id }}">{{ strtoupper($role->role_name) }}</option>
	            		@endforeach
	            	</select>
	            </div>
	            <div class="form-group pull-right">
	            	<button type="submit" class="btn btn-success">Save</button>	
	            </div>
        	</form>
        </div>
    </div>
</div>

@endsection