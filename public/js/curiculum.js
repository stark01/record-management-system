$(document).ready(function(){
    $("#subject_type_id").change(function(){
        let subject_type_id = $(this).val();
        let url = $(this).attr('url')+'/'+ subject_type_id;
        console.log('URL = '+ url)
        if (subject_type_id != '') {
        	$.ajax({
        		url: url,
        		method: "GET",
        		type: "JSON",
        		success:function(data){
        			console.log(data)
                    let option = '<option value ="" selected>--Select Subject---</option>';
                        $.each(data, function(index, value) {
                                option += '<option value ="'+value.id+'">'+value.subject_name+"</option>";
                        });
                    $('#subjects').html(option);
        		}
        	})
        }
        if (subject_type_id == '') {
            let option = '<option value ="" selected>--Select Subject---</option>';
            $('#subjects').html(option);

        }
    })
})