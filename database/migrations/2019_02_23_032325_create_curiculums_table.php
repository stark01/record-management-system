<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuriculumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curiculums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('strand_id')->unsigned()->default(0);
            $table->integer('level_id')->unsigned()->default(0);
            $table->integer('sem_id')->unsigned()->default(0);
            $table->integer('subject_type_id')->unsigned()->default(0);
            $table->integer('subject_id')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curiculums');
    }
}
