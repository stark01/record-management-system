<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_id')->unique();
            $table->string('email_s')->unique();
            $table->string('firstname');
            $table->string('middle_name');
            $table->string('lastname');
            $table->string('birthdate');
            $table->integer('sem_id')->unsigned()->default(0);
            $table->integer('level_id')->unsigned()->default(0);
            $table->integer('strand_id')->unsigned()->default(0);
            $table->string('mother_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('parent_no')->nullable();
            $table->string('guardian')->nullable();
            $table->string('guardian_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}
