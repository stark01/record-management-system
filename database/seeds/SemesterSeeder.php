<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SemesterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('semesters')->insert([
	            'semester_name' => '1st Semester']);
         DB::table('semesters')->insert([
	            'semester_name' => '2nd Semester']);
    }
}
