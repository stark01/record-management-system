<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert([
	            'level_name' => 'Grade 11']);
    	DB::table('levels')->insert([
	            'level_name' => 'Grade 12']);
    }
}
