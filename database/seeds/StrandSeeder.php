<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class StrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        DB::table('strands')->insert([
	            'strand_code' => 'ABM', 
	            'strand_name' => 'Account, Business and Management Strand' 
	        ]);

        DB::table('strands')->insert([
	            'strand_code' => 'GENERAL', 
	            'strand_name' => 'General Academic Strand' 
	        ]);

        DB::table('strands')->insert([
	            'strand_code' => 'HUMSS', 
	            'strand_name' => 'Humanities and Social Sciences Strand' 
	        ]);

        DB::table('strands')->insert([
	            'strand_code' => 'STEM', 
	            'strand_name' => 'Science, Technology, Engineering and Mathematics Strand' 
	        ]);
    	
    	DB::table('strands')->insert([
	            'strand_code' => 'ABM', 
	            'strand_name' => 'TECH-VOC-Information and Communication Technology Programming NC IV' 
	        ]);

    	DB::table('strands')->insert([
	            'strand_code' => 'ABM', 
	            'strand_name' => 'TECH-VOC-Home Economics Cookery NC II and Bread and Pastry NC II' 
	        ]);

    	DB::table('strands')->insert([
	            'strand_code' => 'ABM', 
	            'strand_name' => 'TECH-VOC-Home Economics Health Care Services NC II' 
	        ]);
    }
}
