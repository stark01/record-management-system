<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subject_types')->insert([
	            'subject_type' => 'Core Subjects', 
	        ]);
    	
    	DB::table('subject_types')->insert([
	            'subject_type' => 'Contextualized Subjects', 
	        ]);

    	DB::table('subject_types')->insert([
	            'subject_type' => 'Specialization Subjects', 
	        ]);

    }
}
