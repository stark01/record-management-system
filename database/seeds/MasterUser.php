<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){  
	        DB::table('users')->insert([
	            'name' => 'Master Account',
	            'email' => 'master@master.com',
                'role_id' => 1,
	            'password' => bcrypt('secret123'),
	        ]);
            
    }
}
