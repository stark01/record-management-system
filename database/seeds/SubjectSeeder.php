<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use App\Subject;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,40) as $a) {
        	Subject::create([
        		'subject_type' => $faker->numberBetween($min = 1, $max = 3),
        		'subject_name' => $faker->name.'- TEST SUBJECT ONLY',
        		'subject_hours' => 80
        	]);
        }
    }
}
