<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

Route::get('/', 'LoginController@login')->name('login');
Route::get('/logout', 'LoginController@logout')->name('logout');
Route::post('/validate', 'LoginController@login_validate')->name('validate');


Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function(){
	Route::get('/home', 'HomeController@index')->name('admin');

	Route::get('/user', 'AdminController@user_form')->name('user_form');
	Route::post('/save_user', 'AdminController@save_user')->name('save_user');

	Route::get('/import', 'AdminController@import_blade')->name('import_blade');
	Route::post('/importing', 'AdminController@importing')->name('importing');

	Route::get('/academic-year', 'AdminController@academic_year_form')->name('academic_year_form');
	Route::post('/saving-academic-year', 'AdminController@save_academic_year')->name('save_academic_year');

	Route::get('/activate/{id}', 'AdminController@activate_ac_year')->name('activate_ac_year');

	Route::get('/strand', 'CuriculumController@curiculum')->name('curiculum');
	Route::get('/curiculum', 'CuriculumController@add_curiculum_form')->name('curiculum_form');
	Route::get('/get-subject/{type}', 'CuriculumController@get_subject_by_type')->name('get_subject');
	Route::get('/all-curiculum/{strand_id}', 'CuriculumController@get_curiculum')->name('get_curiculum');

	Route::post('/save-curiculum', 'CuriculumController@save_curiculum')->name('save_curiculum');

	Route::get('/add-student', 'AdminController@add_student')->name('add_student');
	Route::post('/save-student', 'StudentController@save_student')->name('save_student');
	Route::get('/student-list', 'StudentController@student_list')->name('student_list');
	Route::get('/profile/{student_id}', 'StudentController@student_profile')->name('student_profile');
});

